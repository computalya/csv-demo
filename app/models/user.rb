class User < ApplicationRecord
  def self.to_csv(options = {col_sep: ';'})
    CSV.generate(options) do |csv|
      # add header
      column_names = %w(name email)
      csv << column_names
      all.each do |user|
        # csv << user.attributes.values
        # get only data for these columns
        csv << user.attributes.values_at("name", "email")
      end
    end
  end
end