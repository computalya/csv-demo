# README

## create a new demoapp

```bash
rails new csv-demo -B -T
cd csv-demo

bundle install --local

rails g scaffold User name email
rails db:migrate
```

> config/routes.rb

`root 'users#index'`

## add CSV export

> config/application.rb

```ruby
# add before **require "rails"**
require "csv"
```

> app/controllers/users_controller.rb

```ruby
  def index
    @users = User.all

    respond_to do |format|
      format.html
      format.csv do
        #  add BOM to force Excel to realise this file is encoded in UTF-8, so it respects special characters
        # send_data "\uFEFF" + @users.to_csv
        # ANSI code for TR
        send_data ("\uFEFF" + @users.to_csv).encode('iso-8859-9', invalid: :replace, undef: :replace, replace: '')
      end
      # format.csv { send_data @users.to_csv, filename: 'demo.csv' }
    end
  end
```

> app/models/user.rb

**NOTE!**

if you use **;** as seperator, excel add automatically each column

```ruby
class User < ApplicationRecord
  def self.to_csv(options = {col_sep: ';'})
    CSV.generate(options) do |csv|
      # add header
      column_names = %w(name email)
      csv << column_names
      all.each do |user|
        # csv << user.attributes.values
        # get only data for these columns
        csv << user.attributes.values_at("name", "email")
      end
    end
  end
end
```

> app/views/users/index.html.erb

```html
<p>
  Download:
  <%= link_to "CSV", users_path(format: "csv") %>
</p>
```

